package br.com.itau;

import br.com.itau.Carta;

import java.util.*;

public class Baralho {
    private static List<String> listaDeCartasSorteadas;
    private static List<String> baralho;
    private static  String naipe [] = {"copas","ouro","espada","paus"};
    private static  String valor [] = {"A","2","3","4","5","6","7","8","9","10","Q","J","K"};

    public Baralho() {
        baralho=new ArrayList<>();
        listaDeCartasSorteadas= new ArrayList<>();
    }

    public static Carta sorteiaCartaBaralho (){
        boolean cartaFoiSorteada = false;
        int posicaoRandomica;
        int valorCarta;

        Carta carta = null;

        while(!cartaFoiSorteada){
            Random random = new Random();
            posicaoRandomica= random.nextInt(52);

            if(!listaDeCartasSorteadas.contains(baralho.get(posicaoRandomica))){
                valorCarta = valorCarta(posicaoRandomica);
                carta = new Carta(baralho.get(posicaoRandomica), valorCarta);
                listaDeCartasSorteadas.add(baralho.get(posicaoRandomica));
                cartaFoiSorteada=true;
            }
        }

         return carta;
    }

    public static void criaBaralho (){
       String carta;

        for(String val: valor){
            for(String n: naipe){
                carta= val + " de " + n;
                baralho.add(carta);
            }
        }
    }

    public static void imprimeBaralho (){
        for (String carta: baralho){
            System.out.println(carta);
        }
    }

    public static void imprimeCartasSorteadas (){
        for (String carta: listaDeCartasSorteadas){
            System.out.println(carta);
        }
    }

    //public int valorCarta
    public static int valorCarta (int posicao){
        if(posicao > 35) {
            return 10;
        }
        else if(posicao>= 0 && posicao< 4){
            return 1;
        }
        else if(posicao>= 4 &&  posicao < 8){
            return 2;
        }
        else if(posicao>= 8 &&  posicao < 12){
            return 3;
        }
        else if(posicao>= 12 &&  posicao < 16){
            return 4;
        }
        else if(posicao>= 16 &&  posicao < 20){
            return 5;
        }
        else if(posicao>= 20 &&  posicao < 24){
            return 6;
        }
        else if(posicao>= 24 &&  posicao < 28){
            return 7;
        }
        else if(posicao>= 28 &&  posicao < 32){
            return 8;
        }
        else
            return 9;

    }

    public static List<String> getBaralho() {
        return baralho;
    }

    public static void resetListaCartasSorteadas (){
        listaDeCartasSorteadas.clear();
    }
}
