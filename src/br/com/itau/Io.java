package br.com.itau;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.ArrayList;
import java.util.Scanner;

public class Io {

    public static String dadosJogador () {
        Scanner scanner= new Scanner(System.in);
        System.out.println("Olá, digite o seu nome: " );
        String nome  = scanner.nextLine();

        System.out.println("Seja Bem-Vindo(a) ao jogo 21, " +nome);

        return nome;
    }

    public static Integer dadosJogo () {
        Scanner scanner= new Scanner(System.in);
        System.out.println("\nEscolha uma das opções: " +
                           "\n1 para sortear uma nova carta\n2 para iniciar uma nova rodada\n3 finalizar jogo" );

        Integer opcao= scanner.nextInt();

        return opcao;
    }


}
