package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Jogador {
    private String name;
    private static List<Integer> historicoJogadas;
    private static int maiorValorHistorico;
    private  static int pontuacao;

    public Jogador(String name) {
        this.name = name;
        historicoJogadas= new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getHistoricoJogadas() {
        return historicoJogadas;
    }

    public void setHistoricoJogadas() {
        historicoJogadas.add(pontuacao);
    }


    public static void mostrarListaHistorico (){
        int valorRodada;
        for(int i=0; i< historicoJogadas.size(); i++){
            valorRodada= i+1;
            System.out.println("\n*****HISTORICO DA PONTUACAO*****");
            System.out.println("Na rodada " + valorRodada + " : " +historicoJogadas.get(i)) ;
            System.out.println("**********************************");
        }
    }

    public static void setPontuacao  (Carta carta){
        pontuacao = pontuacao + carta.getPontos();
    }

    public static int getPontuacao() {
        return pontuacao;
    }

    public static void zerarPontuacao(){
        pontuacao=0;
    }

    public static void setMaiorValorHistorico(){
        int potuacaoMax =21;
        if(pontuacao > maiorValorHistorico && pontuacao <= potuacaoMax){
            maiorValorHistorico = pontuacao;
        }
    }

    public static int getMaiorValorHistorico() {
        return maiorValorHistorico;
    }
}
