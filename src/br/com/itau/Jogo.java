package br.com.itau;

public class Jogo {
    private Jogador jogador;

    public static void iniciaJogo (){
        String nomeJogador =  Io.dadosJogador();
        Jogador jogador = new Jogador(nomeJogador);

        boolean continuaJogo=true;
        Integer opcao=null;

        Baralho baralho = new Baralho();
        baralho.criaBaralho();
        Carta carta = null;

        while(continuaJogo){
            opcao = Io.dadosJogo();
            if(opcao==1){
                iniciaRodada(carta, baralho, jogador);
                if(jogador.getPontuacao() == 21) {
                    System.out.println("PARABENS, VOCÊ GANHOU A RODADA!");
                }
                else if(jogador.getPontuacao()>21){
                    System.out.println("VOCÊ ESTOUROU OS PONTOS DA RODADA");
                }

            }
            else if(opcao==2){
                resetRodada(jogador, baralho);
                System.out.println("\n\nMaior valor de todas rodadas:" + jogador.getMaiorValorHistorico());

                iniciaRodada(carta, baralho, jogador);
            }
            else {
                System.out.println("Jogo Finalizado");
                System.out.println(("\n\nPontuacao do hitorico: " +jogador.getMaiorValorHistorico()));
                break;
            }
        }

    }


    public static void resetRodada(Jogador jogador, Baralho baralho ){
        jogador.setMaiorValorHistorico();
        jogador.setHistoricoJogadas();
        jogador.zerarPontuacao();
        baralho.resetListaCartasSorteadas();
    }

    public static void iniciaRodada(Carta carta, Baralho baralho, Jogador jogador){
        carta= baralho.sorteiaCartaBaralho();
        System.out.println("Carta sorteada: " +carta.getNome());
        jogador.setPontuacao(carta);
        System.out.println("Pontuacao atual: " +jogador.getPontuacao());
    }
}
